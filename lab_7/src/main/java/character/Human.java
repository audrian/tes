package character;

public class Human extends Player {

    public Human(String name, int health) { super(name, health); }

    public boolean canEat(Player target) {
        return (target instanceof Monster && target.isDead() && target.isBurned());
    }
    public boolean eat(Player target) {
        if (this.canEat(target)) {
            this.diet.add(target);
            this.setHealth(this.getHealth() + 15);
            return true;
        } else {
            return false;
        }
    }
}
//  write Human Class here