package character;

public class Monster extends Player {

    String roar = "AAAAAAaaaAAAAAaaaAAAAAA";

    public Monster(String name, int health) {
        super(name, 2*health);
    }

    public Monster(String name, int health, String roar) {
        super(name, 2*health);
        this.roar = roar;
    }

    public String roar() {
        return this.roar;
    }
}
//  write Monster Class here