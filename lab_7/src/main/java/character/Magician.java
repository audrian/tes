package character;

public class Magician extends Human {

    public Magician(String name, int health) {
        super(name, health);
    }

    public String burn(Player target) {
        super.attack(target);
        if (target.isDead()) {
            target.isBurned = true;
            return String.format("Nyawa %s 0\n dan matang", target.getName());
        } else {
            return String.format("Nyawa %s %d", target.getName(), target.getHealth());
        }
    }
}
//  write Magician Class here