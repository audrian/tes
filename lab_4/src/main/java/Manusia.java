import java.util.List;
import java.util.ArrayList;

public class Manusia {

	//TODO KERJAIN BONUSNYA
	//variabel-variabel
	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan;
	boolean hidup = true;

	//buat daftar manusia
	List<Manusia> daftarManusia = new ArrayList<Manusia>();

	//konstruktor kelas manusia
	public Manusia(String nama, int umur, int uang, float kebahagiaan) {
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = kebahagiaan;
		daftarManusia.add(this);
	}

	public Manusia(String nama, int umur, int uang) {
		this(nama, umur, uang, (float)50);
	}

	public Manusia(String nama, int umur) {
		this(nama, umur, (int)50000, (float)50);
	}

	//setters/getters
	public String getNama() {
		return nama;
	}

	public int getUmur() {
		return umur;
	}

	public int getUang() {
		return uang;
	}

	public void setNama(String namaBaru) {
		this.nama = namaBaru;
	}

	public void setUang(int jumlah) {
		this.uang = jumlah;
		if (this.uang < 0) this.uang = 0;
	}

	public float getKebahagiaan() {
		return kebahagiaan;
	}

	public void setKebahagiaan(float jumlah) {
		this.kebahagiaan = jumlah;
		if (this.kebahagiaan < (float)0) this.kebahagiaan = (float)0;
		else if (this.kebahagiaan > (float)100) this.kebahagiaan = (float)100;
	}

	//method-method manusia
	//TODO
	public void beriUang(Manusia penerima) {
		int ascii = 0;
		for (int i = 0; i < penerima.getNama().length(); i++) {
			ascii += (int)penerima.getNama().charAt(i);
		}
		int jumlah = ascii * 100;
		if (this.getUang() < jumlah) System.out.println("Uang " + this.getNama() + " tidak cukup :(");
		else {
				this.setUang(this.getUang() - jumlah);
				penerima.setUang(penerima.getUang() + jumlah);
				this.setKebahagiaan(this.getKebahagiaan() + (float)jumlah/6000);
				penerima.setKebahagiaan(penerima.getKebahagiaan() + (float)jumlah/6000);
				System.out.println(this.getNama() + " memberikan uang kepada " + penerima.getNama() + " sebesar " + jumlah);
			}
	}

	public void beriUang(Manusia penerima, int jumlah) {
		if (this.getUang() < jumlah) { System.out.println("Uang " + this.getNama() + " tidak cukup :("); }
 		else {
			this.setUang(this.getUang() - jumlah);
			penerima.setUang(penerima.getUang() + jumlah);
			this.setKebahagiaan(this.getKebahagiaan() + (float)jumlah/6000);
			penerima.setKebahagiaan(penerima.getKebahagiaan() + (float)jumlah/6000);
			System.out.println(this.getNama() + " memberikan uang kepada " + penerima.getNama() + " sebesar " + jumlah);
		}
	}

	public void bekerja(int waktu, int beban) {
		try {
			if (this.hidup == false) System.out.println(this.getNama() + " telah berpulang");
			else {
				boolean capek = false; //berguna kalau kebahagian < beban kerja total
				//Cek umur manusia
				if (this.getUmur() < 18) System.out.println (this.getNama() + " masih dibawah umur dan belum bisa bekerja");
				else {
					int bebanTotal = waktu * beban;
					//Cek beban total apakah lebih besar dari kebahagiaan
					if (kebahagiaan < bebanTotal) {
						waktu = (int)this.getKebahagiaan() / beban;
						bebanTotal = waktu * beban;
						capek = true;
					}
					//Kurangi kebahagiaan manusia, tambah uangnya
					int pendapatan = bebanTotal * 10000;
					this.setUang(this.getUang() + pendapatan);
					this.setKebahagiaan(this.getKebahagiaan() - bebanTotal);
					//Output string untuk logging yang cocok
					if (capek) System.out.println(this.getNama() + "tidak bekerja full time karena sudah capek, pendapatan: " + pendapatan);
					else System.out.println(this.getNama() + "bekerja full time, pendapatan: " + pendapatan);
				}
			}
		} catch (IllegalArgumentException e) {
			System.out.println("Syntax: bekerja(int waktu, int beban)");
		}
	}

	public void rekreasi(String tempat) {
		try {
			if (this.hidup == false) System.out.println(this.getNama() + " telah meninggal dunia, kita doakan semoga ia dapat berekreasi di surga nanti");
			else {
				int biaya = tempat.length() * 10000;
				//Cek apakah uang si manusia tidak cukup
				if (this.getUang() < biaya) System.out.println(this.getNama() + " tidak memiliki cukup uang untuk berekreasi di " + tempat);
				else {
					this.setUang(this.getUang() - biaya);
					this.setKebahagiaan(this.getKebahagiaan() + tempat.length());
					System.out.println(this.getNama() + " berekreasi di " + tempat + ", " + this.getNama() + " senang :D");
				}
			}
		} catch (IllegalArgumentException e) {
			System.out.println("Syntax: rekreasi(String tempat)");
		}
	}

	public void sakit(String penyakit) {
		try {
			if (this.hidup == false) System.out.println(this.getNama() + " telah meninggal dunia");
			else {
				this.setKebahagiaan(this.getKebahagiaan() - penyakit.length());
				System.out.println(this.getNama() + " terkena " + penyakit + " :O");
			}
		} catch (IllegalArgumentException e) {
			System.out.println("Syntax: sakit(String penyakit)");
		}
	}

	public void meninggal() {
		if (this.hidup == false) System.out.println(this.getNama() + " tidak bisa meninggal 2 kali");
		else {
			this.hidup = false;
			System.out.println(this.getNama() + " meninggal dengan tenang, kebahagiaan: " + this.getKebahagiaan());
			//wariskan harta
			//jika ia adalah manusia terakhir, semua hartanya hangus
			if (daftarManusia.size() == 1) System.out.println(this.getNama() + " telah meninggal, semua hartanya hangus");
			else {
				this.setUang(0);
				Manusia penerima = daftarManusia.get(daftarManusia.size() - 1); //hartanya diwariskan kepada manusia terakhir yang diinstansiasi
				penerima.setUang(penerima.getUang() + this.getUang());
				}
			}
	}

	public String toString() {
		String fieldNama = this.getNama();
		//Jika sang manusia sudah meninggal tambahkan gelar almarhum
		if (this.hidup == false) fieldNama = "Almarhum " + this.getNama();
		String toPrint = "Nama\t\t : " + fieldNama + "\nUmur\t\t : " + this.getUmur() + "\nUang\t\t : " + this.getUang() + "\nKebahagiaan\t : " + this.getKebahagiaan();
		return toPrint;
	}
}
