import java.util.ArrayList;

public class Korporasi {

    private ArrayList<Karyawan> employees = new ArrayList<Karyawan>();
    private ArrayList<Karyawan> promotedEmployees = new ArrayList<Karyawan>();
    private Integer salaryLimit;

    public Korporasi(Integer salaryLimit) {
        this.salaryLimit = salaryLimit;
    }


    public Karyawan find(String name) {
        for (Karyawan k : this.employees) {
            if (k.getName().equals(name)) {
                return k;
            }
        }
        return null;
    }

    public String tambahKaryawan(String name, String rank, int salary) {
        if (find(name) != null) {
            return "Karyawan dengan nama " + name + " telah terdaftar";
        } else {
            if (rank.equalsIgnoreCase("Intern")) {
                this.employees.add(new Intern(name, salary));
            } else if (rank.equalsIgnoreCase("Staff")) {
                this.employees.add(new Staff(name, salary));
            } else if (rank.equalsIgnoreCase("Manager")) {
                this.employees.add(new Manager(name, salary));
            } else {
                return "Hanya dapat menambah intern/staff/manager";
            }
            return String.format("%s mulai bekerja sebagai %s di PT. TAMPAN", name, rank);
        }
    }

    public String status(String name) {
        if (find(name) == null) {
            return "Karyawan tidak ditemukan";
        } else {
            Karyawan k = find(name);
            return k.getName().toUpperCase() + " " + k.getSalary();
        }
    }

    public String tambahBawahan(String bossName, String subordinateName) {
        Karyawan boss = find(bossName);
        Karyawan subordinate = find(subordinateName);

        if (boss != null && subordinate != null) {
            if (boss.canMakeSubordinate(subordinate) == 1) {
                return "Anda tidak layak memiliki bawahan";
            } else if (boss.canMakeSubordinate(subordinate) == 2) {
                return String.format("%s telah menjadi bawahan %s", bossName, subordinateName);
            } else if (boss.canMakeSubordinate(subordinate) == 3) {
                return String.format("Anda tidak dapat menjadi bawahan diri anda sendiri");
            } else {
                boss.makeSubordinate(subordinate);
                return String.format("Karyawan %s berhasil ditambahkan menjadi bawahan %s", subordinateName, bossName);
            }
        } else {
            return "Nama tidak berhasil ditemukan";
        }
    }

    public boolean canPromote(Karyawan k) {
        return (k instanceof Staff && k.getSalary() > salaryLimit);
    }

    public void gajian() {
        for (Karyawan k : this.employees) {
                k.setTimesPaid(k.getTimesPaid() + 1);

                //Cek kenaikan gaji
                if (k.getTimesPaid() == 6) {
                    int newSalary = k.getSalary() + (int) k.getSalary()/10;
                    System.out.println(String.format("%s mengalami kenaikan gaji sebesar 10%% dari %d ke %d", 
                        k.getName(), k.getSalary(), newSalary));
                    k.setSalary(newSalary);
                    k.setTimesPaid(0);
                }

                //Cek promosi
                if (k instanceof Staff) {
                    if (this.canPromote(k)) {
                        this.promotedEmployees.add(k);
                        this.employees.set(this.employees.indexOf(k), new Manager(k.getName(), k.getSalary()));
                        for (Karyawan l : this.employees) {
                            if (l instanceof Staff || l instanceof Manager) {
                                if (l.removeSubordinate(k)) {
                                    l.removeSubordinate(k);
                                    System.out.println(String.format("%s tidak lagi menjadi bawahan %s", k.getName(), l.getName()));
                                }
                            }
                        }
                    }
                }
            }
        
        System.out.println("Semua karyawan telah digaji");

        for (Karyawan m : this.promotedEmployees) {
            System.out.println(String.format("Selamat, %s telah dipromosikan menjadi MANAGER", m.getName()));
        }

        this.promotedEmployees.clear();
    }
}