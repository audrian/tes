public class Staff extends Manager {

    public Staff(String name, int salary) {
        super(name, salary);
    }

    public int canMakeSubordinate(Karyawan k) {
        //Check if name is identical
        if (k.getName().equalsIgnoreCase(this.getName())) {
            return 1;
        } else {
            //Check potential subordinate rank
            if (!k.getRank().equalsIgnoreCase("Intern")) {
                return 2;
            } else {
                //Check if he's already a subordinate
                if (this.getSubordinates().size() != 0 && this.getSubordinates().contains(k)) {
                    return 3;
                } else {
                    return 0;
                }
            }
        }
    }
}