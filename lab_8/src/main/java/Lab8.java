import java.util.Scanner;

public class Lab8 {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        Integer salaryLimit = s.nextInt();

        Korporasi k = new Korporasi(salaryLimit);

        boolean active = true;

        while (active) {
            String[] input = s.nextLine().split(" ");
            
            String command = input[0];

            if (command.equalsIgnoreCase("TAMBAH_KARYAWAN")) {
                try {
                    String nama = input[1];
                    String pangkat = input[2];
                    int gajiAwal = Integer.parseInt(input[3]);
                    System.out.println(k.tambahKaryawan(nama, pangkat, gajiAwal));

                } catch (Exception e) {
                    System.out.println("Format: TAMBAH_KARYAWAN [nama] [pangkat] [gaji awal]");
                }
            } else if (command.equalsIgnoreCase("STATUS")) {
                try {
                    String nama = input[1];
                    System.out.println(k.status(nama));
                } catch (Exception e) {
                    System.out.println("Format: STATUS [nama]");
                }
            } else if (command.equalsIgnoreCase("TAMBAH_BAWAHAN")) {
                try {
                    String namaBos = input[1];
                    String namaBawahan = input[2];
                    System.out.println(k.tambahBawahan(namaBos, namaBawahan));
                } catch (Exception e) {
                    System.out.println("Format: TAMBAH_BAWAHAN [nama atasan] [nama bawahan]");
                }
            } else if (command.equalsIgnoreCase("GAJIAN")) {
                try {
                    k.gajian();
                } catch (Exception e) {
                    System.out.println("Format: GAJIAN");
                }
            } else {
                System.out.println("Perintah invalid");
            }
            
        }

        s.close();
    }
}