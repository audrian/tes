import java.util.ArrayList;

public class Manager extends Karyawan {

    protected ArrayList<Karyawan> subordinates = new ArrayList<Karyawan>();
    private int subordinatesCount = 0;

    public Manager(String name, int salary) {
        super(name, salary);
    }

    public int getSubordinatesCount() { return this.subordinatesCount; }

    public ArrayList<Karyawan> getSubordinates() { return this.subordinates; }

    public int canMakeSubordinate(Karyawan k) {
        //Check if name is identical
        if (k.getName().equalsIgnoreCase(this.getName())) {
            return 1;
        } else {
            //Check potential subordinate rank
            if (k.getRank().equalsIgnoreCase("Manager")) {
                return 2;
            } else {
                //Check if he's already a subordinate
                if (this.getSubordinates().size() != 0 && this.getSubordinates().contains(k)) {
                    return 3;
                } else {
                    return 0;
                }
            }
        }
    }

    public void makeSubordinate(Karyawan k) {
        if (this.subordinates.size() < 10) {
            this.subordinates.add(k);
        }
    }

    public boolean removeSubordinate(Karyawan k) {
        if (this.subordinates.contains(k)) {
            this.subordinates.remove(k);
            return true;
        }
        return false;
    }
}