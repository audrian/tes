import java.util.Scanner;

public class BingoMain {
	
	public static void main(String[] args) {
		boolean isPlaying = true;
		
		//buat kartu bingo berdasarkan masukan
		Scanner inp = new Scanner(System.in);
		Number[][] numbers = new Number[5][5];
		
		for (int x = 0; x < 5; x++) {
			String[] temp = inp.nextLine().split(" ");
			for (int y = 0; y < 5; y++) {
				numbers[x][y] = new Number(Integer.parseInt(temp[y]), x, y);
			}
		}
		
		Number[] states = new Number[100];
		
		for (int x = 0; x < 5; x++) {
			for (int y = 0; y < 5; y++) {
				states[numbers[x][y].getValue()] = numbers[x][y];
			}
		}
		
		BingoCard card = new BingoCard(numbers, states);
		
		//main loop permainan
		while (isPlaying) {
			String[] command = inp.nextLine().split(" ");
			if (command[0] == "MARK") card.markNum(Integer.parseInt(command[1]));
			else if (command[0] == "INFO") card.info();
			else if (command[0] == "RESTART") card.restart();
			
			if (card.isBingo() == true) isPlaying = false;
		}
		inp.close();
	}
}