/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		//Cek apakah nomor ada di kartu
		String output = "";
		if (numberStates[num] == null) {
			output = "Kartu tidak memiliki angka " + num;
		} else {
			if (numberStates[num].isChecked()) {
				output = num + " sebelumnya sudah tersilang";
			} else {
				numberStates[num].setChecked(true);
				output = num + " tersilang";
			}
		}
		checkBingo();
		return output;
	}	
	
	public boolean checkBingo() {
		int bingoCount;
		//Cek bingo horizontal
		for (int i = 0; i < 5; i++) {
			bingoCount = 0;
			for (int j = 0; j < 5; j++) {
				if (numbers[j][i].isChecked()) bingoCount += 1;
			}
			if (bingoCount == 5) setBingo(true);
		}
		
		//Cek bingo vertikal
		for (int i = 0; i < 5; i++) {
			bingoCount = 0;
			for (int j = 0; j < 5; j++) {
				if (numbers[i][j].isChecked()) bingoCount += 1;
			}
			if (bingoCount == 5) setBingo(true);
		}
		
		//Cek bingo miring
		if ((numbers[0][0].isChecked() &&
			numbers[1][1].isChecked() &&
			numbers[2][2].isChecked() &&
			numbers[3][3].isChecked() &&
			numbers[4][4].isChecked()) ||
			(numbers[0][4].isChecked() &&
			numbers[1][3].isChecked() &&
			numbers[2][2].isChecked() &&
			numbers[3][1].isChecked() &&
			numbers[4][0].isChecked())) setBingo(true);
			
		return isBingo();
	}
	
	public String info(){
		String output = "";
		for (int i = 0; i < 5; i++) {
			output += "| ";
			for (int j = 0; j < 5; j++) {
				if (numbers[i][j].isChecked()) output += "X ";
				else output += numbers[i][j].getValue();
				if (j != 4) output += " | ";
				else output += " |";
				}
			if (i != 4) output += "\n";
		}
		return output;
	}
	
	public void restart(){
		for(int i = 0; i < 100; i++) {
			if(numberStates[i] != null) {
				if (numberStates[i].isChecked()) {
					numberStates[i].setChecked(false);
				}
			}
		}
		System.out.println("Mulligan!");
	}
}
