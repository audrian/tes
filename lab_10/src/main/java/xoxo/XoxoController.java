package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Muhammad Audrian AP
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Result of all previous decryptions.
     */
    private String decryptLog = "";

    /**
     * Result of all previous encryptions.
     */
    private String encryptLog = "";

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        //TODO: Write your code for logic and everything here
        gui.setDecryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                decrypt();
            }
        });

        gui.setEncryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                encrypt();
            }
        });

        gui.setWriteToFileFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { generateOutputFile(); }
        });
    }

    //TODO: Create any methods that you want
    void encrypt() {
        try {
            XoxoEncryption encryptor = new XoxoEncryption(gui.getKeyText().trim());
            String message = gui.getMessageText().trim().replaceAll("\\s","");
            int seed = Integer.parseInt(gui.getSeedText());
            String result = encryptor.encrypt(message).getEncryptedMessage();
            encryptLog += result;
            gui.appendLog("Encryption successful!!");
        } catch (SizeTooBigException e) {
            gui.appendLog("Encryption failed: File size exceeds 10Kbits");
        } catch (RangeExceededException e) {
            gui.appendLog("Encryption failed: Key length exceeds 28 characters");
        } catch (InvalidCharacterException e) {
            gui.appendLog("Encryption failed: Message contains invalid character");
        }
    }

    void decrypt() {
        try {
            XoxoDecryption decryptor = new XoxoDecryption(gui.getKeyText().trim());
            gui.appendLog("Key: " + gui.getKeyText().trim());
            String message = gui.getMessageText().trim();
            gui.appendLog("Message: " + message);
            int seed = Integer.parseInt(gui.getSeedText());
            String result = decryptor.decrypt(message, seed);
            decryptLog += result + "\n";
            gui.appendLog("Decryption successful");
            gui.appendLog("Decrypted message: " + result);
        } catch (SizeTooBigException e) {
            gui.appendLog("Decryption failed: File size exceeds 10Kbits");
        } catch (InvalidCharacterException e) {
            gui.appendLog("Decryption failed: Character must be A-Z, a-z, or 0-9");
        } catch (RangeExceededException e) {
            gui.appendLog("Decryption failed: Key length exceeds 28 characters");
        }
    }

    void generateOutputFile() {
        String encryptOutPath = System.getProperty("user.dir") + "/out/encrypt.enc";
        String decryptOutPath = System.getProperty("user.dir") + "/out/decrypt.txt";
        File encryptedFile = new File(encryptOutPath);
        File decryptedFile = new File(decryptOutPath);

        try {
            if (encryptedFile.createNewFile()) {
                gui.appendLog("Encrypted output file not found -- creating new file");
            } else {
                gui.appendLog("Encrypted output file found -- appending");
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter(encryptOutPath, true));
            writer.write(encryptLog);
            gui.appendLog("Encrypted file successfully written");
            writer.close();
            if (decryptedFile.createNewFile()) {
                gui.appendLog("Decrypted output file not found -- creating new file");
            } else {
                gui.appendLog("Decrypted output file found -- appending");
            }
            writer = new BufferedWriter(new FileWriter(decryptOutPath, true));
            writer.write(decryptLog);
            gui.appendLog("Decrypted file successfully written");
            writer.close();
        } catch (IOException e) {
            gui.appendLog(e.getMessage());
        }
    }
}