package lab9.event;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Event {

    private String name;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private BigInteger cost;

    public Event(String name, LocalDateTime startDate, LocalDateTime endDate, BigInteger cost) {
        this.name = name;
        this.cost = cost;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public String getName() {
        return name;
    }

    public BigInteger getCost() {
        return cost;
    }

    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss");

        return String.format(
                "%s\nWaktu mulai: %s\nWaktu selesai: %s\nBiaya kehadiran: %d",
                this.name, formatter.format(this.startDate), formatter.format(this.endDate), this.cost);
    }
}
