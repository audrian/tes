package lab9;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import lab9.event.*;
import lab9.user.*;

public class EventSystem {

    private ArrayList<User> users;
    private ArrayList<Event> events;

    public EventSystem() {
        users = new ArrayList<>();
        events = new ArrayList<>();
    }

    public User getUser(String name) {
        for (User u : users) {
            if (u.getName().equals(name)) return u;
        }

        return null;
    }

    public Event getEvent(String name) {
        for (Event e : events) {
            if (e.getName().equals(name)) {
                return e;
            }
        }

        return null;
    }

    boolean userExists(String name) {
        for (User u : users) {
            if (u.getName().equals(name)) return true;
        }

        return false;
    }

    boolean eventExists(String name) {
        for (Event e : events) {
            if (e.getName().equals(name)) return true;
        }

        return false;
    }

    public String addUser(String name) {
        //Check username availability
        if (userExists(name)) {
            return String.format("User %s sudah ada!", name);
        }

        users.add(new User(name));
        return String.format("User %s berhasil ditambahkan!", name);
    }

    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");

        try {
            LocalDateTime start = LocalDateTime.from(formatter.parse(startTimeStr));
            LocalDateTime end = LocalDateTime.from(formatter.parse(endTimeStr));

            //Verify time
            if (start.isAfter(end)) {
                return "Waktu yang diinputkan tidak valid!";
            } else {
                //Verify name
                if (eventExists(name)) {
                    return String.format("Event %s sudah ada!", name);
                } else {
                    //Add event
                BigInteger cost = new BigInteger(costPerHourStr);
                events.add(new Event(name, start, end, cost));
                return String.format("Event %s berhasil ditambahkan!", name);
                }
            }
        } catch (DateTimeParseException e) {
            e.printStackTrace();
        }

        return "Event gagal ditambahkan karena ada kesalahan";
    }

    public String registerToEvent(String userName, String eventName) {
        if (!userExists(userName) && !eventExists(eventName)) {
            return String.format("Tidak ada pengguna dengan nama %s dan acara dengan nama %s!", userName, eventName);
        } else if (!userExists(userName)) {
            return String.format("Tidak ada pengguna dengan nama %s!", userName);
        } else if (!eventExists(eventName)) {
            return String.format("Tidak ada acara dengan nama %s!", eventName);
        } else {
            User user = getUser(userName);
            Event event = getEvent(eventName);

            if (user.addEvent(event)) {
                user.addEvent(event);
                return String.format("%s berencana menghadiri %s!", userName, eventName);
            } else {
                return String.format("%s sibuk sehingga tidak dapat menghadiri %s!", userName, eventName);
            }
        }
    }
}
