package movie;
import ticket.Ticket;
import theater.Theater;
import customer.Customer;
public class Movie {
	
	private String title;
	private String rating;
	private int duration;
	private String genre;
	private String status;
	
	public Movie(String title, String rating, int duration, String genre, String status) {
		this.title = title;
		this.rating = rating;
		this.duration = duration;
		this.genre = genre;
		this.status = status;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public int getMinimumAge() {
		if (rating.equals("Umum")) return 0;
		else if (rating.equals("Remaja")) return 13;
		else if (rating.equals("Dewasa")) return 17;
		else return -1; //Throws error
	}
	
	public int getDuration() {
		return this.duration;
	}
	
	public String getGenre() {
		return this.genre;
	}
	
	public String getStatus() {
		return this.status;
	}
	
	public String getRating() {
		return this.rating;
	}
	
	public void printInfo() {
		String output =
			"------------------------------------------------------------------" +
			"\nJudul   : " + this.getTitle() +
			"\nGenre   : " + this.getGenre() +
			"\nDurasi  : " + this.getDuration() +
			"\nRating  : " + this.rating +
			"\nStatus  : Film " + this.getStatus() +
			"\n------------------------------------------------------------------";
		
		System.out.println(output);
	}
	
}
