package customer;
import movie.Movie;
import ticket.Ticket;
import theater.Theater;
public class Customer {
	
	private String name;
	private int age;
	private boolean isFemale;
	
	public Customer(String name, String gender, int age) {
		this.name = name;
		this.age = age;
		this.isFemale = gender.equals("Perempuan") ? true : false;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public boolean getGender() {
		return this.isFemale;
	}
	
	public void findMovie(Theater theater, String title) {
		Movie[] availableMovies = theater.getAvailableMovies();
		
		boolean isAvailable = false;
		//Check movie availability
		for (Movie movie : availableMovies) {
			if (movie.getTitle().equals(title)) {
				movie.printInfo();
			isAvailable = true;
			break;
			}
		}
		
		//Print error message
		if (!isAvailable) {
			System.out.println(
				"Film " + title +
				" yang dicari " + this.getName() +
				" tidak ada di bioskop " + theater.getName());
		}
	}
	
	public Ticket orderTicket(Theater theater, String title, String day, String type) {
		Ticket query = null;
		
		for (int i = 0; i < theater.getAvailableTickets().size(); i++) {
            //Create comparator ticket
            Ticket comparator = theater.getAvailableTickets().get(i);
            String compTitle = comparator.getMovie().getTitle();
            String compDay = comparator.getDay();
            String compType = comparator.getType();
            
            //Compare values
            if (compTitle.equals(title) &&
                compDay.equals(day) &&
                compType.equals(type)) {
                    query = comparator;
                }
            }
            
            if (query == null) {
                //Print error message
                System.out.println("Tiket untuk film " + title +
                                   " jenis " + type +
                                   " dengan jadwal " + day +
                                   " tidak tersedia di " + theater.getName());
            } else {
                if (query.getMovie().getMinimumAge() > this.getAge()) {
                    System.out.println(this.getName() + " masih belum cukup umur untuk menonton " +
                                       query.getMovie().getTitle() + " dengan rating " +
                                       query.getMovie().getRating());
                } else {
                    System.out.println(this.getName() + " telah membeli tiket " +
                                       query.getMovie().getTitle() + " jenis " +
                                       query.getType() + " di " +
                                       theater.getName() + " pada hari " +
                                       query.getDay()+ " seharga Rp. "
                                       + query.getPrice());
                    theater.setBalance(theater.getBalance() + query.getPrice());
                    return query;
                }
            }
        return null;
	}
}
