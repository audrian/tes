//Rabbit House (Lab 3)
//Author: Muhammad Audrian AP - NPM 1706043790 - DDP 2 E

import java.util.Scanner;

public class RabbitHouse {
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		String tmp = input.nextLine();
		try {
		String cmd = tmp.split(" ")[0]; //normal atau palindrom
		String arg = tmp.split(" ")[1]; //string yang akan dicek
			if(cmd.equalsIgnoreCase("normal")) {
				System.out.println(countRabbits(arg.length()));
			} else if(cmd.equalsIgnoreCase("palindrom")) {
				System.out.println(countPalindromes(arg));
			}
		} catch (ArrayIndexOutOfBoundsException e) { //jika perintah tidak ditemukan
			System.out.println("Perintah tidak diketahui");
			System.out.println("Syntax: <normal/palindrom> <string>");
		}
		input.close();
	}
	
	static int countRabbits(int num) {
		if(num > 1) {
			return 1 + num * countRabbits(num-1);
		} else {
			return 1;
		}
	}
	
	static boolean isPalindrome(String str) {
		if (str.length() <= 1) {
			return true;
		} else {
			return str.equals(new StringBuilder(str).reverse().toString());
		}
	}
	
	static int countPalindromes(String str) {
		int res;
		if(isPalindrome(str) == true) {
			return 0;
		} else {
			res = 1;
			for (int i = 0; i < str.length(); i++) {
				res += countPalindromes(str.substring(0, i) + str.substring(i+1));
			}
		}
		return res;
	}			
}