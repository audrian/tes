public class Iseng {

    public static void main(String[] args) {

        String tes = "lalalaLA012";

        System.out.println(tes.toCharArray());

        for (char c : tes.toCharArray()) {
          System.out.println(c);
        }

        for (int i = 0; i < tes.length(); i++) {
          System.out.println("Digit: " + Character.isDigit(tes.charAt(i)));
          System.out.println("Letter: " + Character.isLetter(tes.charAt(i)));
        }
    }
}