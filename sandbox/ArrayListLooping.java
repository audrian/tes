import java.util.ArrayList;

public class ArrayListLooping {
	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(1);
		al.add(2);
		al.add(3);
		al.add(4);
		al.add(5);
		
		for (Integer i : al) {
			System.out.println(i);
		}
	}
}
		