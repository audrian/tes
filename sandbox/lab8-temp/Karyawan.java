public class Karyawan {

    private String name;
    private int salary;
    private int timesPaid = 0;

    public Karyawan(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() { return this.name; }

    public int getSalary() { return this.salary; }

    public String getRank() { return this.getClass().getSimpleName(); }

    public int getTimesPaid() { return this.timesPaid; }

    public void pay() { 
        this.salary += this.salary/10;
        this.timesPaid++; 
    }

    public boolean canBePromoted() { return false; }

    public boolean canMakeSubordinate(Karyawan k) { return false; }

    public void makeSubordinate(Karyawan k) {}

}