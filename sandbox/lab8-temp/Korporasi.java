import java.util.ArrayList;

public class Korporasi {

    private ArrayList<Karyawan> employees = new ArrayList<Karyawan>();
    private int employeeCount = 0;

    public Karyawan find(String name) {
        for (Karyawan k : this.employees) {
            if (k.getName().equals(name)) {
                return k;
            }
        }
        return null;
    }

    public String tambahKaryawan(String name, String rank, int salary) {
        if (find(name) == null) {
            return String.format("Karyawan dengan nama %s telah terdaftar", name);
        } else {
            int count = this.employeeCount;
            if (rank.equals("Intern")) {
                this.employees.add(new Intern(name, salary));
            } else if (rank.equals("Staff")) {
                this.employees.add(new Staff(name, salary));
            } else if (rank.equals("Manager")) {
                this.employees.add(new Manager(name, salary));
            } else {
                return "Hanya ada tiga jenis posisi: Staff, Intern dan manager";
            }
            this.employeeCount++;
            return String.format("%s mulai bekerja sebagai %s di PT. TAMPAN", name, rank);
        }
    }

    public String status(String name) {
        if (find(name) == null) {
            return "Karyawan tidak ditemukan";
        } else {
            Karyawan k = find(name);
            return k.getName().toUpperCase() + " " + k.getSalary();
        }
    }

    public String tambahBawahan(String bossName, String subordinateName) {
        Karyawan boss = find(bossName);
        Karyawan subordinate = find(subordinateName);

        if (boss != null && subordinate != null) {
            if (!boss.canMakeSubordinate(subordinate)) {
                return "Anda tidak layak memiliki bawahan";
            } else {
                boss.makeSubordinate(subordinate);
                return String.format("Karyawan %s telah menjadi bawahan %s", subordinateName, bossName);
            }
        } else {
            return "Nama tidak berhasil ditemukan";
        }
    }

    public void gajian() {
        for (int i = 0; i < this.employees.size() - 1; i++) {
            if (this.employees.get(i) == null) {
                continue;
            } else {
                if (this.employees.get(i).canBePromoted()) {
                    promosi(this.employees.get(i));
                    String name = this.employees.get(i).getName();
                    System.out.println(String.format("Selamat, %s telah dipromosikan menjadi MANAGER", name));
                }
            }
        }
        System.out.println("Semua karyawan telah diberikan gaji");
    }

    public void promosi(Karyawan k) {
        if (k instanceof Staff) {
            k = new Manager(k.getName(), k.getSalary());
        }
    }
}