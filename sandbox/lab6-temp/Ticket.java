package ticket;
import movie.Movie;
import theater.Theater;
import customer.Customer;
public class Ticket {
	private Movie movie;
	private int price;
	private boolean is3D;
	private String day;
	
	private static final int STARTING_PRICE = 60000;
	
	public Ticket(Movie movie, String day, boolean is3D) {
		this.movie = movie;
		this.day = day;
		this.is3D = is3D;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public Movie getMovie() {
		return this.movie;
	}
	
	public String getDay() {
		return this.day;
	}
	
	public String getType() {
		if (this.is3D) return "3 Dimensi";
		else if (!this.is3D) return "Biasa";
		else return null;
	}
	
	public void adjustPrice() {
		if (day.equals("Sabtu") || day.equals("Minggu")) this.price += 40000;
		if (is3D) this.price += this.price / 5; //Price becomes 20% higher
	}
	
	public void printInfo() {
		String type = "Biasa";
		if (is3D) type = "3 Dimensi";
		
		String output =
			"------------------------------------------------------------------" +
			"\nFilm            : " + this.movie.getTitle() +
			"\nJadwal Tayang   : " + this.day +
			"\nJenis           : " + type +
			"\n------------------------------------------------------------------";
		
		System.out.println(output);
	}
		
	public boolean equals(Ticket comparator) {
		return (this.getMovie().getTitle().equals(comparator) &&
				this.getDay().equals(comparator) &&
				this.getType().equals(comparator));
	}
}