package theater;
import movie.Movie;
import ticket.Ticket;
import customer.Customer;
import java.util.Arrays;
import java.util.ArrayList;

public class Theater {
	private String name;
	private int balance;
	private Movie[] movies;
	private ArrayList<Ticket> tickets;
	
	public Theater(String name, int balance, ArrayList<Ticket> tickets, Movie[] movies) {
		this.name = name;
		this.balance = balance;
		this.movies = movies;
		this.tickets = tickets;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getBalance() {
		return this.balance;
	}
	
	public void setBalance(int amount) {
		this.balance = amount;
	}
	
	public ArrayList<Ticket> getAvailableTickets() {
		return this.tickets;
	}
	
	public Movie[] getAvailableMovies() {
		return this.movies;
	}
	
	public Movie getMovie(String title) {
        for (Movie movie : movies) {
            if (movie.getTitle().equals(title)) {
                return movie;
            }
        }
        return null;
    }
	
	public void printInfo() {
		//Create movie list
		String movieList = "";
		for (int i = 0; i < this.getAvailableMovies().length; i++) {
			movieList += this.getAvailableMovies()[i].getTitle();
			if (i != this.getAvailableMovies().length - 1) movieList += ", ";
		}
		
		String output =
			"------------------------------------------------------------------" +
			"\nBioskop                 : " + this.getName() +
			"\nSaldo Kas               : " + this.getBalance() +
			"\nJumlah Tiket Tersedia   : " + this.tickets.size() +
			"\nDaftar Film Tersedia    : " + movieList +
			"\n------------------------------------------------------------------";
		
		System.out.println(output);
	}
	
	public static void printTotalRevenueEarned(Theater[] theaters) {
		//Count total revenue
		int revenue = 0;
		for (Theater theater : theaters) {
			revenue += theater.getBalance();
		}
		
		System.out.println(
			" Total uang yang dimiliki Koh Mas : Rp. " + revenue +
			"\n------------------------------------------------------------------");
			for (Theater theater : theaters) {
				System.out.println(
					"\nBioskop         : " + theater.getName() +
					"\nSaldo Kas       : " + theater.getBalance());
			}
		System.out.println(
			"------------------------------------------------------------------");
	}
}
