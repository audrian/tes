import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUIPrototype {

    private JFrame mainFrame;
    private JPanel panel1, panel2, panel3;
    private JTextField p1tf;
    private JLabel p2label;
    private JButton p3but;
    private String p1tfval;

    public static void main(String[] args) {
        new GUIPrototype();
    }

    public GUIPrototype() {
        mainFrame = new JFrame("Xoxo prototype");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    

        initPanels();

        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    void initPanels() {
        panel1 = new JPanel();
        initPanel1();

        panel2 = new JPanel();
        initPanel2();
        
        panel3 = new JPanel();
        initPanel3();

        
    }

    void initPanel1() {
        panel1.add(new JLabel("Panel 1"));
        p1tf = new JTextField("Type your message here...");

        panel1.add(p1tf);
        mainFrame.add(panel1, BorderLayout.NORTH);
    }

    void initPanel2() {
        panel2.add(new JLabel("This is your message: "));
        p2label = new JLabel("");
        mainFrame.add(panel2, BorderLayout.CENTER);
    }

    void initPanel3() {
        p3but = new JButton("Click here and see what happens");
        p3but.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                this.p2label.setText(p1tf.getText());
            }
        });
        panel3.add(p3but);
        mainFrame.add(panel3, BorderLayout.SOUTH);
    }
}