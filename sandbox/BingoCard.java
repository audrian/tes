import java.util.Scanner;

public class BingoCard {
	
	//Initialize array
	private String[][] card = new String[5][5];
	
	//Constructor
	public void BingoCard() {
		Scanner input = new Scanner(System.in);
		
		//Fill array with user input
		for (int i = 0; i < 5; i++) {	//Fill vertical (x-axis)
			String[] temp = input.nextLine().split(" ");
			
			for (int j = 0; j < 5; j++) {   //Fill horizontal (y-axis)
				setValue(j, i, temp[j]);
			}
		}
		String[][] copy = card.clone(); //The actual card used in gameplay
	}

	//Setters & getters
	public String getValue(int xpos, int ypos) {
		return card[xpos][ypos];
	}
	
	public void setValue(int xpos, int ypos, String value) {
		card[xpos][ypos] = value;
	}	
	
	//Main methods
	public void markNum() {
		//Marks specified number with "X" if it exists
		//todo
	}
	
	public void info() {
		//Displays current state of bingo card
		//todo
	}
	
	public void restart() {
		//Restarts game
		//todo
	}
}