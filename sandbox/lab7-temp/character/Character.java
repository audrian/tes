//Muhammad Audrian - 1706043790
//Contact me @ rianfiftyseven (at) gmail (dot) com

//Superclass untuk semua makhluk di game

package character;
import java.util.ArrayList;

public class Character {

    private String name;
    private int hp;
    private ArrayList<Character> diet;
    protected boolean isBurned;

    public Character(String name, int hp) {
        this.name = name;
        this.hp = hp;
    }

    public String getName() { 
        return this.name; 
    }

    public int getHealth() { 
        if (this.hp < 0) this.hp = 0;
        return this.hp; 
    }

    public void setHealth(int hp) { 
        this.hp = hp; 
    }

    public boolean isDead() { 
        return (this.hp == 0); 
    }

    public boolean isBurned() {
        return this.isBurned;
    }

    public void attack(Character target, int damage) {
        if (!this.isDead()) {
            target.setHealth(target.getHealth() - damage);
        } else {
            System.out.println(this.getName() + " tidak bisa menyerang " + target.getName());
        }
    }

    public void eat(Character target) {
        if (this.isDead()) {
            if (target.isDead()) {
                this.diet.add(target);
                this.setHealth(this.getHealth() + 15);
            }
        } else {
            System.out.println(this.getName() + " tidak bisa memakan " + target.getName());
        }
    }

    public void diet() {
        if (!diet.isEmpty()) {
            for (Character c : diet) {
                System.out.print(c.getClass().getSimpleName() + " " + c.getName() + ", ");
            }
        } else {
            System.out.println("Belum memakan siapa siapa")
        }
    }

    public void status() {
        System.out.println(this.getClass().getSimpleName() + " " + this.getName());
        System.out.println("HP: " + this.getHealth());

        if (this.isDead()) System.out.println("Sudah meninggal dunia dengan damai");
        else System.out.println("Masih hidup");

        System.out.println("Memakan ");
        this.diet();
    }

}
