import character.*;

public class Helper {

    public static void main(String[] args) {

        Game g = new Game();

        System.out.println(g.add("Bongo", "Magician", 30));
        System.out.println(g.add("Bongo", "Monster", 100));
        System.out.println(g.add("Bingo", "Monster", 45));
        
        System.out.println("ALL PLAYERS\n---");
        System.out.println(g.status());

        System.out.println(g.attack("Bongo", "Bingo"));
        System.out.println(g.eat("Bingo", "Bongo"));

        for (int i = 0; i < 8; i++) {
            System.out.println(g.burn("Bongo", "Bingo"));
        }

        System.out.println(g.eat("Bongo", "Bingo"));
    }
}