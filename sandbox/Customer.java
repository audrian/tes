import java.util.ArrayList;
import java.util.Arrays;

public class Customer {
	private String name;
	private int age;
	private boolean isFemale;
	private ArrayList<Ticket> inventory;
	
	public Customer(String name, boolean isFemale, int age) {
		this.name = name;
		this.age = age;
		this.isFemale = isFemale;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public boolean getGender() { 
		//Returns true if customer is female
		return this.isFemale;
	}
	
	public void setGender(boolean isFemale) {
		this.isFemale = isFemale;
	}
	
	public void orderTicket(Theater theater, String title, String schedule, String type) {
		//Dummy comparator
		Ticket query = theater.getTicket(title, schedule, type);
		
		//Verify availability
		if (query == null) {
			System.out.println("Filmnya ga main :("); //DEBUG
		} else {
			//Verify age
			if (this.getAge() < query.getMovie().getMinimumAge()) {
				System.out.println("Belum cukup umur :("); //DEBUG
			} else {
				inventory.add(query);
				theater.setRevenue(theater.getRevenue() + query.getPrice());
				System.out.println("Pembelian tiket berhasil!"); //DEBUG
			}
		}
	}
					
}